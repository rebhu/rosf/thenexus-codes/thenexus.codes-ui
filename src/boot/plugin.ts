import { boot } from 'quasar/wrappers'
import Vue from 'vue'
declare module 'particles.vue'
import Particles from 'particles.vue'

Vue.use(Particles)
// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default boot(async (/* { app, router, Vue ... } */) => {
  // something to do
})
